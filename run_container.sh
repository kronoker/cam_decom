#!/usr/bin/env bash

IMAGE_NAME="ibd:latest"

USER_NAME=$( whoami )
CONTAINER_NAME=${USER_NAME}-ibd

WORK_DIR="/ibd"

docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}

nvidia-docker run -it -d --net=host --ipc=host \
-v ${HOME}/IBD:${WORK_DIR} \
-v /mnt/DATA/${USER_NAME}/broden:/data \
-w ${WORK_DIR} --name ${CONTAINER_NAME} ${IMAGE_NAME} bash
