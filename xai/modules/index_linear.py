# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F

from typing import Optional

__all__ = ["IndexLinear"]


class IndexLinear(nn.Linear):

    def __init__(self, in_features: int, out_features: int, bias: bool = True):
        super().__init__(in_features, out_features, bias)

    def forward(self, x: torch.Tensor, idx: Optional[int] = None) -> torch.Tensor:
        if idx is not None:
            return F.linear(x, self.weight[idx:idx + 1], self.bias[idx:idx + 1])
        else:
            return F.linear(x, self.weight, self.bias)

    def unbiased_forward(self, x: torch.Tensor, idx: Optional[int] = None) -> torch.Tensor:
        weight = self.weight.data  # [num_concepts, 512]
        weight -= weight.mean(1)[:, None]
        if idx is not None:
            return F.linear(x, weight[idx:idx + 1], self.bias[idx:idx + 1])
        else:
            return F.linear(x, weight, self.bias)
