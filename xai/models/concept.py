# -*- coding: utf-8 -*-

import torch

from typing import Dict, Optional, Tuple

from xai.modules.index_linear import IndexLinear
from xai.models.base import Model, OutputType, Forwarder, LossCalculator
from xai.hyperparams import Hyperparams
from xai.data import Batch
from xai.losses import LossType, CompositeLoss

__all__ = ["ConceptClassifier", "ConceptForwarder", "ConceptLossCalculator"]


class ConceptClassifier(Model):

    def __init__(self, hparams: Hyperparams):
        super().__init__(hparams)
        self.index_linear = IndexLinear(hparams.clf_feature_size, hparams.num_concepts)

    def default_forward(self, x: torch.Tensor, concept_id: Optional[int] = None) -> Dict[OutputType, torch.Tensor]:
        return {
            OutputType.MODEL_OUTPUT: self.index_linear.forward(x, idx=concept_id),
        }

    def unbiased_forward(self, x: torch.Tensor, concept_id: Optional[int] = None) -> Dict[OutputType, torch.Tensor]:
        return {
            OutputType.MODEL_OUTPUT: self.index_linear.unbiased_forward(x, idx=concept_id),
        }


class ConceptForwarder(Forwarder):

    def forward_train(self, model: ConceptClassifier, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return model.default_forward(batch.x, *args, **kwargs)

    def forward_val(self, model: ConceptClassifier, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return self.forward_train(model, batch, *args, **kwargs)

    def forward_test(self, model: ConceptClassifier, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return model.unbiased_forward(batch.x, *args, **kwargs)


class ConceptLossCalculator(LossCalculator):

    def __init__(self, hparams: Hyperparams, loss: CompositeLoss):
        super().__init__(hparams)
        self.loss = loss

    def calc_train(self, fwd_res: Dict[OutputType, torch.Tensor], batch: Batch,
                   epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        model_output = fwd_res[OutputType.MODEL_OUTPUT]
        loss_input = {
            LossType.MSE: {
                'input': model_output,
                'target': batch.y,
            },
            LossType.BCE: {
                'input': model_output,
                'target': batch.y,
            }
        }
        return self.loss.calculate(loss_input, epoch)

    def calc_val(self, fwd_res: Dict[OutputType, torch.Tensor], batch: Batch,
                 epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        return self.calc_train(fwd_res, batch, epoch)
