# -*- coding: utf-8 -*-

import torch
import torch.nn as nn

from typing import Dict
from torchvision.models.resnet import BasicBlock, Bottleneck

from xai.models.base import Model, OutputType, Forwarder
from xai.hyperparams import Hyperparams
from xai.data.batch import Batch

__all__ = ["ResNet", "ResNetForwarder"]


class ResNet(Model):

    def __init__(self, hparams: Hyperparams):
        super().__init__(hparams)

        if hparams.basic_block_name == "BasicBlock":
            self.block_cls = BasicBlock
        elif hparams.basic_block_name == "Bottleneck":
            self.block_cls = Bottleneck
        else:
            raise ValueError("Invalid 'basic_block_name'. Should be 'BasicBlock' or 'Bottleneck'")

        self.dilation = 1
        self.inplanes = 64

        self.conv1 = nn.Conv2d(3, self.inplanes, 7, stride=2, padding=3, bias=False)

        self.bn1 = nn.BatchNorm2d(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(3, stride=2, padding=1)

        self.layer1 = self.__make_layer(64, hparams.layers_conf[0])
        self.layer2 = self.__make_layer(128, hparams.layers_conf[1], stride=2)
        self.layer3 = self.__make_layer(256, hparams.layers_conf[2], stride=2)
        self.layer4 = self.__make_layer(512, hparams.layers_conf[3], stride=2)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        self.fc = nn.Linear(512 * self.block_cls.expansion, hparams.num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def __make_layer(self, planes: int, blocks: int, stride: int = 1):
        downsample = None
        if stride != 1 or self.inplanes != planes * self.block_cls.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * self.block_cls.expansion, 1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * self.block_cls.expansion),
            )

        layers = []
        block = self.block_cls(self.inplanes, planes, stride=stride, downsample=downsample, norm_layer=nn.BatchNorm2d)
        layers.append(block)
        self.inplanes = planes * self.block_cls.expansion
        for _ in range(1, blocks):
            layers.append(self.block_cls(self.inplanes, planes, norm_layer=nn.BatchNorm2d))

        return nn.Sequential(*layers)

    # we expect that x with dimensions [B, 3, 224, 224]
    def default_forward(self, x: torch.Tensor) -> Dict[OutputType, torch.Tensor]:
        x = self.conv1(x)  # [B, 3, 224, 224] -> [B, 64, 112, 112]
        x = self.bn1(x)
        x = self.relu(x)

        x = self.maxpool(x)  # [B, 64, 112, 112] -> [B, 64, 56, 56]

        x = self.layer1(x)
        x = self.layer2(x)  # [B, 64, 56, 56] -> [B, 128, 28, 28]
        x = self.layer3(x)  # [B, 128, 28, 28] -> [B, 256, 14, 14]
        last_fmap = self.layer4(x)  # [B, 256, 14, 14] -> [B, 512, 7, 7]

        x = self.avgpool(last_fmap).flatten(1)  # [B, 512, 7, 7] -> [B, 512, 1, 1] -> [B, 512]
        x = self.fc(x)  # [B, 512] -> [B, num_classes]

        return {
            OutputType.MODEL_OUTPUT: x,
            OutputType.LAST_FEATURE_MAP: last_fmap,
        }


class ResNetForwarder(Forwarder):

    def forward_train(self, model: ResNet, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return model.default_forward(batch.x, *args, **kwargs)

    def forward_val(self, model: ResNet, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return self.forward_train(model, batch, *args, **kwargs)

    def forward_test(self, model: Model, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return self.forward_train(model, batch, *args, **kwargs)
