# -*- coding: utf-8 -*-

import torch
import logging

from enum import Enum
from typing import Dict, Tuple

from xai.data import Batch
from xai.hyperparams import Hyperparams
from xai.losses.composite_loss import CompositeLoss

logger = logging.getLogger(__name__)

__all__ = ["OutputType", "Model", "Forwarder", "LossCalculator"]


class OutputType(Enum):
    """
    Different output types constants
    """
    MODEL_OUTPUT = "model_output"
    LAST_FEATURE_MAP = "last_feature_map"


class Model(torch.nn.Module):
    """
    Base class for any models
    """

    def __init__(self, hparams: Hyperparams):
        super().__init__()
        self.hparams = hparams

    def forward(self, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        return self.default_forward(*args, **kwargs)

    def default_forward(self, x: torch.Tensor) -> Dict[OutputType, torch.Tensor]:
        raise NotImplementedError("No 'default_forward' implementation")

    def count_parameters(self) -> int:
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    def get_mem_usage(self) -> int:
        """
        :return: model memory usage in bytes
        """
        mem_params = sum([param.nelement() * param.element_size() for param in self.parameters()])
        mem_bufs = sum([buf.nelement() * buf.element_size() for buf in self.buffers()])
        return mem_params + mem_bufs

    def load_state_dict(self, state_dict: Dict[str, torch.Tensor], strict: bool = True) -> None:
        keys = list(state_dict.keys())
        for k in keys:
            if k.startswith("module."):
                new_k = k[len("module."):]
                state_dict[new_k] = state_dict.pop(k)

        # Добавим в state_dict все ключи модели, которых там нет.
        self_state_dict = self.state_dict()
        for k, v in self_state_dict.items():
            if k not in state_dict:
                state_dict[k] = v
                logger.info(f'use default initialization for module {k}')

        # И выкинем все те ключи, которых нет в модели.
        keys_to_remove = []
        for k, v in state_dict.items():
            if k not in self_state_dict:
                keys_to_remove.append(k)

        for k in keys_to_remove:
            state_dict.pop(k)
            logger.info(f'module {k} dropped from loaded checkpoint')

        super().load_state_dict(state_dict, strict)


class Forwarder:
    """
    Base class for data forwarding
    """
    def __init__(self, hparams: Hyperparams):
        self.hparams = hparams

    def forward_train(self, model: Model, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        raise NotImplementedError("No forward implementation for training")

    def forward_val(self, model: Model, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        raise NotImplementedError("No forward implementation for validation")

    def forward_test(self, model: Model, batch: Batch, *args, **kwargs) -> Dict[OutputType, torch.Tensor]:
        raise NotImplementedError("No forward implementation for testing")


class LossCalculator:
    """
    Base class for losses calculations
    """
    def __init__(self, hparams: Hyperparams):
        self.hparams = hparams

    def calc_train(self, fwd_res: Dict[OutputType, torch.Tensor], b: Batch,
                   epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        raise NotImplementedError("No loss calculation implementation for training")

    def calc_val(self, fwd_res: Dict[OutputType, torch.Tensor], b: Batch,
                 epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        raise NotImplementedError("No loss calculation implementation for validation")

    def cpu(self) -> "LossCalculator":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, (torch.nn.Module, CompositeLoss)):
                self_dict[attr] = value.cpu()
        return self

    def cuda(self, **kwargs) -> "LossCalculator":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, (torch.nn.Module, CompositeLoss)):
                self_dict[attr] = value.cuda(**kwargs)
        return self
