# -*- coding: utf-8 -*-

import cv2
import PIL
import attr
import copy
import logging
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.utils.tensorboard import SummaryWriter
from typing import Dict, Tuple, List
from PIL.Image import Image
from PIL import ImageDraw, ImageFont
from sklearn.metrics import roc_curve, auc, average_precision_score

from xai.hyperparams import Hyperparams
from xai.data import Batch, Dictionary
from xai.models import ConceptClassifier


__all__ = [
    "LossMetrics", "ConceptMetrics", "EpochMetrics", "MetricsWriter", "ConceptMetricsCalculator",
    "EpochMetricsCalculator", "DecompositionCalculator"
]

logger = logging.getLogger(__name__)


@attr.s
class LossMetrics:
    phase = ""
    concept_name = ""

    losses = attr.ib(default={}, type=Dict[str, float])

    def __add__(self, other: 'LossMetrics') -> 'LossMetrics':
        res = copy.deepcopy(self)
        for k, v in other.losses.items():
            if k not in res.losses:
                res.losses[k] = 0.
            res.losses[k] += v
        return res

    def __truediv__(self, other: int) -> 'LossMetrics':
        res = copy.deepcopy(self)
        for k, v in res.losses.items():
            res.losses[k] = v / other
        return res


@attr.s
class ConceptMetrics:
    phase = ""
    concept_name = ""

    accuracy = attr.ib(type=float)  # threshold = 0.5
    auc_roc = attr.ib(type=float)
    avg_precision = attr.ib(type=float)


@attr.s
class EpochMetrics:
    grads_total_norm = attr.ib(type=float)
    weights_total_norm = attr.ib(type=float)

    grads_norm_per_layer = attr.ib(type=Dict[str, float])
    weights_norm_per_layer = attr.ib(type=Dict[str, float])

    epoch_time = attr.ib(type=float)
    lr = attr.ib(type=float)

    mean_avg_precision = attr.ib(type=float)
    mean_auc_roc = attr.ib(type=float)
    mean_accuracy = attr.ib(type=float)


class EpochMetricsCalculator:

    def __init__(self, model: nn.Module):
        self.model = model

    def calc(self, concept_metrics_list: List[ConceptMetrics], epoch_time: float, lr: float) -> EpochMetrics:
        grad_norm_per_layer, total_grad_norm = self.__calc_grad_norm()
        weights_norm_per_layer, total_weights_norm = self.__calc_weights_norm()

        mean_avg_precision = np.mean([metrics.avg_precision for metrics in concept_metrics_list]).item()
        mean_auc_roc = np.mean([metrics.auc_roc for metrics in concept_metrics_list]).item()
        mean_accuracy = np.mean([metrics.accuracy for metrics in concept_metrics_list]).item()

        return EpochMetrics(
            grads_total_norm=total_grad_norm, weights_total_norm=total_weights_norm, epoch_time=epoch_time,
            grads_norm_per_layer=grad_norm_per_layer, weights_norm_per_layer=weights_norm_per_layer, lr=lr,
            mean_accuracy=mean_accuracy, mean_avg_precision=mean_avg_precision, mean_auc_roc=mean_auc_roc
        )

    def __calc_grad_norm(self, norm_type: int = 2) -> Tuple[Dict[str, float], float]:
        norm_per_layer = {}
        total_norm = 0

        for name, value in self.model.named_parameters():
            if value.grad is None:
                continue
            param_norm = value.grad.data.norm(norm_type)
            norm_per_layer[name] = param_norm
            total_norm += param_norm.item() ** norm_type

        total_norm = total_norm ** (1. / norm_type)

        return norm_per_layer, total_norm

    def __calc_weights_norm(self, norm_type: int = 2) -> Tuple[Dict[str, float], float]:
        norm_per_layer = {}
        total_norm = 0

        for param_name, param_value in self.model.named_parameters():
            param_norm = param_value.data.norm(norm_type)
            norm_per_layer[param_name] = param_norm
            total_norm += param_norm.item() ** norm_type
        total_norm = total_norm ** (1. / norm_type)

        return norm_per_layer, total_norm


class ConceptMetricsCalculator:

    def __init__(self, hparams: Hyperparams):
        self.hparams = hparams
        self.__scores = None
        self.__labels = None
        self.reset()

    def reset(self) -> None:
        self.__scores = torch.empty(0, dtype=torch.float32)
        self.__labels = torch.empty(0, dtype=torch.long)
        if self.hparams.use_cuda:
            self.__scores = self.__scores.cuda()
            self.__labels = self.__labels.cuda()

    def update(self, predictions: torch.Tensor, batch: Batch) -> None:
        # we expect that passed scores and labels in the shape [B, 1]
        self.__scores = torch.cat([self.__scores, predictions.squeeze(1)])
        self.__labels = torch.cat([self.__labels, batch.y.squeeze(1)])

    def calc(self) -> ConceptMetrics:
        labels = self.__labels.cpu().numpy()
        scores = self.__scores.cpu().numpy()

        fpr, tpr, thresholds = roc_curve(labels, scores, drop_intermediate=False)
        avg_precision = average_precision_score(labels, scores)
        auc_value = auc(fpr, tpr)
        accuracy = (((labels == 1) & (scores >= 0.5)) | ((labels == 0) & (scores < 0.5))).sum().item()
        accuracy /= len(labels)

        self.reset()

        return ConceptMetrics(accuracy=accuracy, auc_roc=auc_value, avg_precision=avg_precision)


class DecompositionCalculator:

    def __init__(self, hparams: Hyperparams, class_predictor: nn.Module, concept_dictionary: Dictionary,
                 class_dictionary: Dictionary):
        self.hparams = hparams
        self.concept_dictionary = concept_dictionary
        self.class_dictionary = class_dictionary

        self.predictor_weight = list(class_predictor.parameters())[-2].data.cpu()
        self.predictor_weight = F.normalize(self.predictor_weight, dim=1)
        self.concept_clf_weight = None  # [num_concepts, 512]

        self.ranks = None  # [365, basis_num]
        self.err_var = None  # [365, basis_num]
        self.coeff = None  # [365, basis_num + 2]
        self.residuals = None  # [365, num_concepts]

    def calc(self, batch: Batch, concept_logit: torch.Tensor) -> list:
        # we expect that concept_logit has size [B, 7, 7, num_concepts]
        concept_logit = concept_logit.cpu().numpy()
        concept_clf_weight = self.concept_clf_weight.numpy()

        # [B, 7, 7, 512] * [B, 1, 1, 512] -> [B, 7, 7, 512]
        mat = (batch.x * batch.grad_x.mean((1, 2))[:, None, None]).cpu().numpy()
        image = batch.image.cpu().numpy().transpose(0, 2, 3, 1)  # [B, 224, 224, 3]

        pred_indices = batch.probs.argmax(1)  # [B, 365] -> [B]
        ranks = self.ranks[pred_indices].numpy()  # [B, basis_num]
        residuals = self.residuals[pred_indices].numpy()  # [B, 512]
        coeff = self.coeff[pred_indices].numpy()  # [B, basis_num + 2]

        a = F.normalize(batch.x.mean((1, 2)), dim=1).cpu().numpy()  # [B, 7, 7, 512] -> [B, 512]

        result_images = []

        for sample_idx in range(batch.size):
            image_one = image[sample_idx]  # [224, 224, 3]
            img_cam = self.cam_mat(mat[sample_idx], above_zero=False)  # [7, 7]
            vis_cam = self.vis_cam_mask(img_cam, image_one)  # [224, 224, 3]

            coeff_one = coeff[sample_idx][:self.hparams.basis_num, None]  # [basis_num + 2] -> [basis_num, 1]
            ranks_one = ranks[sample_idx]  # [basis_num]
            residuals_one = residuals[sample_idx]  # [512]

            # vstack([basis_num, 512], [1, 512]) -> [basis_num + 1, 512]
            component_weights = np.vstack([
                coeff_one * concept_clf_weight[ranks_one],  # [basis_num, 1] * [basis_num, 512] -> [basis_num, 512]
                residuals_one[None, :]  # [1, 512]
            ])

            qcas = (component_weights @ a[sample_idx])  # [basis_num + 1, 512] @ [512] -> [basis_num + 1]
            comb_score = qcas.sum().item()  # [basis_num + 1] -> scalar

            inds = qcas[:-1].argsort()[:-self.hparams.basis_num - 1:-1]  # [basis_num + 1] -> [basis_num]
            concept_masks_ind = ranks_one[inds]  # [basis_num] -> [basis_num]
            scores_topn = coeff[sample_idx, inds]  # [B, basis_num + 2] -> [basis_num]
            contribution = qcas[inds]  # [basis_num + 1] -> [basis_num]

            # [B, 7, 7, num_concepts] -> [7, 7, basis_num]
            concept_masks = concept_logit[sample_idx][:, :, concept_masks_ind]
            # [7, 7, basis_num] * [1, 1, basis_num] -> [7, 7, basis_num]
            concept_masks = concept_masks * ((scores_topn > 0) * 1)[None, None, :]
            concept_masks = (np.maximum(concept_masks, 0)) / np.max(concept_masks)  # [7, 7, basis_num]

            vis_concept_cam = []
            for basis_idx in range(self.hparams.basis_num):
                vis_cam_comp = self.vis_cam_mask(concept_masks[:, :, basis_idx], image_one)  # [224, 224, 3]
                vis_concept_cam.append(vis_cam_comp)

            vis_img = PIL.Image.fromarray(image_one)
            vis_bm = self.big_margin(224, self.hparams.font_path, self.hparams.font_size)
            image_list = [vis_img, vis_cam, vis_bm] + vis_concept_cam[:3]
            vis = self.concat_images(image_list, margin=self.hparams.margin)

            captions = []
            for i in range(3):
                concept_name = self.concept_dictionary.get_name_by_index(concept_masks_ind[i])
                captions.append("%s(%4.2f%%)" % (concept_name, contribution[i] * 100 / comb_score))

            class_name = self.class_dictionary.get_name_by_label(pred_indices[sample_idx].item())
            captions = ["%s(%.2f) " % (class_name, comb_score)] + captions

            vis_headline = self.headline(captions, 224, vis.height // 5, vis.width, self.hparams.font_path,
                                         self.hparams.font_size, margin=self.hparams.margin)
            vis = self.stack_images([vis_headline, vis])
            result_images.append(vis)

        return result_images

    def calculate_basis(self, concept_classifier: ConceptClassifier) -> None:
        concept_clf_weight = concept_classifier.index_linear.weight.data
        concept_clf_weight -= concept_clf_weight.mean(1).unsqueeze(1)
        self.concept_clf_weight = F.normalize(concept_clf_weight, dim=1).cpu()  # [num_concepts, 512]

        basis_num = self.hparams.basis_num

        # [365, basis_num]
        self.ranks = torch.zeros((self.predictor_weight.shape[0], basis_num), dtype=torch.int32)
        # [365, basis_num + 2]
        self.coeff = torch.zeros((self.predictor_weight.shape[0], basis_num + 2))
        # [365, 512]
        self.residuals = torch.zeros((self.predictor_weight.shape[0], self.concept_clf_weight.shape[1]))

        for class_idx in range(self.predictor_weight.shape[0]):
            resid = self.predictor_weight[class_idx].clone()  # [512]
            ortho_concepts = self.concept_clf_weight.clone()  # [num_concepts, 512]
            ortho_indices = torch.arange(self.concept_clf_weight.shape[0])

            # [512, basis_num + 2]
            basis = torch.zeros((self.predictor_weight.shape[1], basis_num + 2))

            for basis_idx in range(basis_num):
                # [num_concepts, 512] @ [512] -> [num_concepts]
                argmax_index = (ortho_concepts @ self.predictor_weight[class_idx]).argmax().item()
                best_index = ortho_indices[argmax_index].item()

                resid -= ortho_concepts[argmax_index] * (ortho_concepts[argmax_index] @ resid)  # [512]
                basis[:, basis_idx] = self.concept_clf_weight[best_index]
                self.ranks[class_idx, basis_idx] = best_index

                # [100, 512] @ [512] -> [100] ; torch.outer([100], [512]) -> [100, 512]
                ortho_concepts -= (ortho_concepts @ ortho_concepts[argmax_index]).outer(ortho_concepts[argmax_index])
                ortho_concepts = F.normalize(ortho_concepts, dim=1)

                indices = torch.cat([
                    torch.arange(argmax_index),
                    torch.arange(argmax_index + 1, ortho_concepts.shape[0]),
                ])
                ortho_concepts = ortho_concepts[indices]
                ortho_indices = ortho_indices[indices]

            basis[:, basis_num] = F.normalize(torch.maximum(resid, torch.tensor(0)), dim=0)
            basis[:, basis_num + 1] = F.normalize(-torch.minimum(resid, torch.tensor(0)), dim=0)

            self.residuals[class_idx] = resid
            # [basis_num + 2, 512] @ [512] -> [basis_num + 2]
            self.coeff[class_idx] = basis.pinverse() @ self.predictor_weight[class_idx]

    @staticmethod
    def big_margin(vis_size: int, font_path: str, font_size: int) -> Image:
        w, h = vis_size // 3, vis_size
        canvas = PIL.Image.fromarray(np.full((h, w, 3), 255, dtype=np.int8), mode="RGB")
        draw = ImageDraw.Draw(canvas)
        font = ImageFont.truetype(font_path, font_size)
        draw.text(((w - font_size * 0.5) / 2, (vis_size - font_size * 1.8) / 2), "=", font=font, fill=(0, 0, 0, 255))
        return canvas

    @staticmethod
    def cam_mat(mat: np.ndarray, above_zero: bool = False) -> torch.Tensor:
        # [7, 7, 512]
        if above_zero:
            mat = np.maximum(mat, 0)
        if mat.ndim == 3:
            mat = mat.sum(2)  # [7, 7, 512] -> [7, 7]
        mat = mat - np.min(mat)  # [7, 7] - scalar -> [7, 7]
        mat /= np.max(mat)  # [7, 7] / scalar -> [7, 7]
        return mat

    @staticmethod
    def concat_images(images: List[Image], margin: int = 0) -> List[Image]:
        w = sum([img.width for img in images])
        ret = PIL.Image.new("RGB", (w + (len(images) - 1) * margin, images[0].height), color=(255, 255, 255))
        w_pre = 0
        for i, image in enumerate(images):
            ret.paste(image, (w_pre + margin * int(bool(i)), 0))
            w_pre += image.width + margin * int(bool(i))
        return ret

    @staticmethod
    def vis_cam_mask(cam_mat: np.ndarray, image: np.ndarray) -> np.ndarray:
        # we expect that cam_mat has size [7, 7] and image has size [224, 224, 3]
        cam_mask = cv2.resize(cam_mat, image.shape[:2])
        cam_mask = (cam_mask * 255).astype(np.uint8)
        cam_mask = cv2.applyColorMap(cam_mask, cv2.COLORMAP_JET)[:, :, ::-1]  # [224, 224, 3]
        vis_cam = (cam_mask * 0.5 + image * 0.5).astype(np.uint8)  # [224, 224, 3]
        vis_cam = PIL.Image.fromarray(vis_cam)
        return vis_cam

    @staticmethod
    def headline(captions: List[str], vis_size: int, height: int, width: int, font_path: str, font_size: int,
                 margin: int = 3) -> Image:
        vis_headline = PIL.Image.fromarray(np.full((height, width, 3), 255, dtype=np.int8), mode="RGB")
        draw = ImageDraw.Draw(vis_headline)
        font = ImageFont.truetype(font_path, font_size)

        for idx in range(len(captions)):
            caption = captions[idx]
            fw, fh = draw.textsize(caption)
            if idx == 0:
                draw.text(((vis_size * 2 - fw * 1.85) / 2, (height - fh * 1.85) / 2), caption, font=font,
                          fill=(0, 0, 0, 255))
            else:
                coord = (vis_size * 7 // 3 + (vis_size + margin) * (idx - 1), 0)
                draw.text((coord[0] + (vis_size - fw * 1.85) / 2, coord[1] + (height - fh * 1.85) / 2), caption,
                          font=font, fill=(0, 0, 0, 255))

        return vis_headline

    @staticmethod
    def stack_images(images: List[Image]) -> Image:
        h = sum([img.height for img in images])
        ret = PIL.Image.new("RGB", (images[0].width, h))
        h_pre = 0
        for i, image in enumerate(images):
            ret.paste(image, (0, h_pre))
            h_pre += image.height
        return ret


class MetricsWriter:
    """
    Чтобы не засорять Trainer, функциональность логирования метрик вынесли в этот класс.
    """

    def __init__(self, log_dir: str):
        self.summary_writer = SummaryWriter(log_dir=log_dir)

    def __log_metrics_dict(self, global_step: int, metrics_dict: Dict[str, float], tag: str) -> None:
        for metric_name, metric_value in metrics_dict.items():
            self.summary_writer.add_scalar(f"{tag}/{metric_name}", metric_value, global_step)

    def log_loss_metrics(self, epoch: int, metrics: LossMetrics) -> None:
        if metrics is None:
            return

        self.__log_metrics_dict(epoch, metrics.losses, f"{metrics.phase}/{metrics.concept_name}")

    def log_images(self, epoch: int, images: List[Image]) -> None:
        for idx, image in enumerate(images):
            tensor = torch.from_numpy(np.array(image).transpose((2, 0, 1)))
            self.summary_writer.add_image(f"decomposition/{idx}", tensor, global_step=epoch)

    def log_concept_metrics(self, epoch: int, metrics: ConceptMetrics) -> None:
        if metrics is None:
            return

        for metric_name, metric_values in attr.asdict(metrics).items():
            tag = f"{metrics.phase}/{metrics.concept_name}/{metric_name}"
            if isinstance(metric_values, (float, int)):
                self.summary_writer.add_scalar(tag, metric_values, epoch)
            elif isinstance(metric_values, dict):
                self.__log_metrics_dict(epoch, metric_values, tag)

    def log_epoch_metrics(self, epoch: int, metrics: EpochMetrics) -> None:
        if metrics is None:
            return

        self.summary_writer.add_scalar(f'Epoch/weights_total_norm', metrics.weights_total_norm, epoch)
        self.summary_writer.add_scalar(f'Epoch/grads_total_norm', metrics.grads_total_norm, epoch)

        self.__log_metrics_dict(epoch, metrics.weights_norm_per_layer, f"Epoch/weights_norm")
        self.__log_metrics_dict(epoch, metrics.grads_norm_per_layer, f"Epoch/grads_norm")

        self.summary_writer.add_scalar('Epoch/epoch_time', metrics.epoch_time, epoch)
        self.summary_writer.add_scalar('Epoch/lr', metrics.lr, epoch)

        self.summary_writer.add_scalar('Epoch/mean_average_precision', metrics.mean_avg_precision, epoch)
        self.summary_writer.add_scalar('Epoch/mean_auc_roc', metrics.mean_auc_roc, epoch)
        self.summary_writer.add_scalar('Epoch/mean_accuracy', metrics.mean_accuracy, epoch)
