# -*- coding: utf-8 -*-

import torch
import random
import logging
import numpy as np

from typing import Optional

logger = logging.getLogger(__name__)

__all__ = ["set_random_seed", "get_random_seed"]

_seed: Optional[int] = None


def set_random_seed(seed: int = 987) -> None:
    global _seed
    _seed = seed
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.cuda.random.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    logger.info(f'deterministic mode, seed={seed}')


def get_random_seed() -> int:
    global _seed
    return _seed
