# -*- coding: utf-8 -*-

import os
import lmdb
import torch
import pickle
import numpy as np
import torch.nn.functional as F

from typing import Optional, Dict
from tqdm.auto import tqdm
from collections import defaultdict
from torch.utils.data import DataLoader

from xai.hyperparams import Hyperparams
from xai.models.base import Model, Forwarder, OutputType
from xai.data.batch import Batch

__all__ = ["Featurizer"]


class Featurizer:

    def __init__(self, hparams: Hyperparams, dataloader: DataLoader, class_predictor: Model, forwarder: Forwarder,
                 out_dir: str, concept_idx_path: Optional[str] = None):
        self.hparams = hparams
        self.dataloader = dataloader
        self.class_predictor = class_predictor.eval()
        self.forwarder = forwarder

        self.lmdb_path = os.path.join(out_dir, "lmdb", "data.lmdb")
        self.__check_dir(self.lmdb_path)
        self.env = lmdb.Environment(self.lmdb_path, map_size=1024 ** 4, subdir=False, readahead=False, max_dbs=0,
                                    writemap=False, meminit=False, map_async=True, lock=False)

        self.concept_idx_path = concept_idx_path
        if concept_idx_path is not None:
            self.rel_path = os.path.relpath(self.lmdb_path, os.path.dirname(concept_idx_path))
            self.__check_dir(concept_idx_path)
            self.index = defaultdict(list)

    def __del__(self):
        self.close()

    def close(self) -> None:
        self.env.close()

    @staticmethod
    def __check_dir(path: str) -> None:
        folder = os.path.dirname(path)
        if folder and not os.path.isdir(folder):
            os.makedirs(folder, exist_ok=True)

    def run(self) -> None:
        self.__run()
        if self.concept_idx_path is not None:
            self.__write_index()

    def __write_index(self) -> None:
        with open(self.concept_idx_path, 'wb') as __fout:
            __fout.write(pickle.dumps(self.index))

    def __run(self) -> None:
        if self.hparams.use_cuda:
            self.class_predictor = self.class_predictor.cuda()

        for batch in tqdm(self.dataloader):
            self.class_predictor.zero_grad()

            if self.hparams.use_cuda:
                batch = batch.cuda()

            model_outputs = self.forwarder.forward_val(self.class_predictor, batch)
            self.__process_output(model_outputs, batch)

    def __process_output(self, model_res: Dict[OutputType, torch.Tensor], batch: Batch) -> None:
        features = model_res[OutputType.LAST_FEATURE_MAP]  # [B, D, H, W]
        h, w = features.shape[2:]

        probs = model_res[OutputType.MODEL_OUTPUT].softmax(1)  # [B, 365]

        features.retain_grad()

        onehot = torch.zeros_like(probs).scatter(1, probs.argmax(1).unsqueeze(1), 1.)  # [B, 365]
        probs.backward(onehot)

        grad_features = features.grad  # [B, D, H, W]

        features = features.detach().cpu().numpy()
        grad_features = grad_features.cpu().numpy()
        probs = probs.detach().cpu().numpy()

        image = batch.image.cpu().numpy()  # [3, 224, 224]
        # [B, 112, 112]
        annot = batch.annot.cpu() if batch.annot is not None else None

        with self.env.begin(write=True) as tnx:
            for sample_idx in range(batch.size):
                lmdb_key = batch.paths[sample_idx].split(":")[1]
                lmdb_value = {
                    "features": features[sample_idx],
                    "grad_features": grad_features[sample_idx],
                    "image": image[sample_idx],
                    "probs": probs[sample_idx],
                }
                tnx.put(lmdb_key.encode(), pickle.dumps(lmdb_value), dupdata=False, overwrite=True)

                if self.concept_idx_path is None:
                    continue

                for concept in np.unique(annot[sample_idx]):
                    if concept == 0:
                        continue
                    mask = (annot[sample_idx] == concept).type(torch.float32).unsqueeze(0)
                    downsampled = F.adaptive_max_pool2d(mask, (h, w)).squeeze(0).bool()
                    for i in range(h):
                        for j in range(w):
                            if downsampled[i, j]:
                                self.index[concept].append((f"{self.rel_path}:{lmdb_key}", i, j))
