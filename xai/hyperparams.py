# -*- coding: utf-8 -*-

import os
import attr

from typing import List, Tuple


@attr.s
class Hyperparams:

    # training params
    use_cuda = attr.ib(True, type=bool)
    batch_size = attr.ib(32, type=int)
    num_workers = attr.ib(4, type=int)
    lr = attr.ib(1., type=float)
    n_copy_train_set = attr.ib(1, type=int)

    # ResNet params
    basic_block_name = attr.ib("BasicBlock", type=str)  # BasicBlock or Bottleneck
    layers_conf = attr.ib([2, 2, 2, 2], type=List[int])  # Number of block of each layer
    num_classes = attr.ib(2, type=int)  # Number of classes
    cnn_feature_map_size = attr.ib((7, 7), type=Tuple[int, int])  # Last feature map size of Resnet

    # Concept classifier params
    num_concepts = attr.ib(100, type=int)  # Number of concepts
    clf_feature_size = attr.ib(100, type=int)  # Classifier input size

    # Weight decomposition params
    basis_num = attr.ib(7, type=int)  # how many concepts are used to interpret the weight vector of a class
    font_path = attr.ib(os.path.join("/ibd/resources/font.ttc"), type=str)
    font_size = attr.ib(26, type=int)
    margin = attr.ib(7, type=int)
