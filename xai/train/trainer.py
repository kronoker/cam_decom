# -*- coding: utf-8 -*-

import os
import time
import torch
import pickle
import logging
import numpy as np

from typing import Tuple, Dict, Optional, List, Callable, Set
from tqdm.auto import tqdm
from operator import itemgetter
from itertools import product
from torch.optim.optimizer import Optimizer
from torch.optim.lr_scheduler import _LRScheduler
from torch.utils.data import DataLoader, Dataset
from sklearn.model_selection import train_test_split

from xai.models import LossCalculator, Forwarder, OutputType, Model
from xai.data import ConceptDataset, Collator, Dictionary, Batch
from xai.hyperparams import Hyperparams
from xai.utils import (
    TrainerCheckpointParams, LossMetrics, MetricsWriter, CheckpointManager, ConceptMetricsCalculator,
    get_random_seed, EpochMetricsCalculator, ConceptMetrics, DecompositionCalculator
)

logger = logging.getLogger(__name__)

__all__ = ["Trainer", "ConceptLoaderFactory"]


class ConceptLoaderFactory:

    def __init__(self, index_path: str, hparams: Hyperparams, concept_dictionary: Dictionary,
                 worker_initer: Optional[Callable] = None):
        self.hparams = hparams
        self.worker_initer = worker_initer

        with open(index_path, 'rb') as __fin:
            data = pickle.loads(__fin.read())

        self.__concept2data = dict()
        for concept, value in data.items():
            if concept_dictionary.has_label(concept):
                for idx in range(len(value)):
                    path, i, j = value[idx]
                    value[idx] = (os.path.join(os.path.dirname(index_path), path), i, j)
                self.__concept2data[concept] = value

        logger.info(f"Number of concepts will be processed: {len(self.__concept2data)}")

        f_size = hparams.cnn_feature_map_size
        image_keys = set()
        for data in self.__concept2data.values():
            for triplet in data:
                image_keys.add(triplet[0])

        logger.info(f"Number of image features will be processed: {len(image_keys)}")

        self.__total = []
        key2index = dict()
        idx_product = list(product(range(f_size[0]), range(f_size[1])))

        for idx, image_key in enumerate(image_keys):
            key2index[image_key] = f_size[0] * f_size[1] * idx
            self.__total.extend([(image_key, *pair) for pair in idx_product])

        self.__concept2neg_indices = dict()  # neg indices
        all_indices = set(range(len(self.__total)))

        for concept, data in self.__concept2data.items():
            pos_indices = set(key2index[image_key] + i * f_size[0] + j for image_key, i, j in data)
            self.__concept2neg_indices[concept] = list(all_indices - pos_indices)

        self.neg_cache: Dict[int, List[np.ndarray]] = dict()  # first item - indices ; second - scores

        self.__concepts = list(map(itemgetter(0), sorted(self.__concept2data.items(), key=lambda pair: -len(pair[1]))))

    def get_concepts(self) -> Set[int]:
        return set(self.__concepts)

    def get_topk_concepts(self, k: int = 10) -> Set[str]:
        return set(self.__concepts[:k])

    def set_neg_cache(self, cache: Dict[int, List[np.ndarray]]) -> None:
        self.neg_cache = cache

    def negative_mining_loader(self, split_ratio: int = 3, sample_ratio: int = 2,
                               shuffle: bool = True) -> Tuple[Dict[int, DataLoader], Dict[int, DataLoader]]:
        train_loaders = dict()
        valid_loaders = dict()

        for concept, pos_samples in tqdm(self.__concept2data.items()):
            neg_indices, neg_scores = self.neg_cache[concept]

            if len(neg_scores) == 0:
                sample_indices = np.random.choice(len(neg_indices), len(pos_samples) * sample_ratio, replace=False)
            else:
                sample_indices = neg_scores.argsort()[:-len(pos_samples) * sample_ratio - 1:-1]

            neg_indices = neg_indices[sample_indices]
            neg_samples = list(itemgetter(*neg_indices)(self.__total))

            train_dataset, valid_dataset = self.__make_datasets(neg_samples, pos_samples, split_ratio=split_ratio)
            train_loader = DataLoader(dataset=train_dataset, shuffle=shuffle, batch_size=self.hparams.batch_size,
                                      collate_fn=Collator(), num_workers=self.hparams.num_workers,
                                      worker_init_fn=self.worker_initer)
            train_loaders[concept] = train_loader

            if valid_dataset is not None:
                valid_loader = DataLoader(dataset=valid_dataset, shuffle=shuffle, collate_fn=Collator(),
                                          batch_size=self.hparams.batch_size, num_workers=self.hparams.num_workers,
                                          worker_init_fn=self.worker_initer)
                valid_loaders[concept] = valid_loader

        return train_loaders, valid_loaders

    def negative_test_concept_loader(self, sample_ratio: int = 20) -> Dict[int, DataLoader]:
        loaders = dict()

        if self.neg_cache is not None:
            logger.info(f"Negative cache is not empty and contains {len(self.neg_cache)} concepts")

        for concept, pos_samples in tqdm(self.__concept2data.items()):
            if concept not in self.neg_cache:
                neg_indices = self.__concept2neg_indices[concept]
                pos_count = len(self.__concept2data[concept])
                neg_indices = np.random.choice(neg_indices, min(len(neg_indices), sample_ratio * pos_count),
                                               replace=False)
                self.neg_cache[concept] = [neg_indices, np.empty(0, dtype=np.float32)]
            else:
                neg_indices = self.neg_cache[concept][0]

            neg_samples = list(itemgetter(*neg_indices)(self.__total))
            loader = DataLoader(dataset=ConceptDataset(neg_samples, [0] * len(neg_samples)), shuffle=False,
                                collate_fn=Collator(), batch_size=self.hparams.batch_size,
                                num_workers=self.hparams.num_workers, worker_init_fn=self.worker_initer)
            loaders[concept] = loader

        return loaders

    @staticmethod
    def __make_datasets(neg: List[Tuple[str, int, int]], pos: List[Tuple[str, int, int]],
                        split_ratio: int = 3) -> Tuple[Dataset, Dataset]:
        labels = [0] * len(neg) + [1] * len(pos)
        x_train, x_valid, y_train, y_valid = train_test_split(neg + pos, labels, stratify=labels, shuffle=True,
                                                              train_size=split_ratio / (split_ratio + 1),
                                                              random_state=get_random_seed())

        return ConceptDataset(x_train, y_train), ConceptDataset(x_valid, y_valid)


class Trainer:

    def __init__(self, hparams: Hyperparams, loader_factory: ConceptLoaderFactory, test_loader: DataLoader,
                 model: Model, class_predictor: Model, concept_dictionary: Dictionary, class_dictionary: Dictionary,
                 loss_calculator: LossCalculator, forwarder: Forwarder, optimizer: Optimizer, scheduler: _LRScheduler,
                 checkpoint_manager: CheckpointManager, metrics_writer: MetricsWriter):

        self.hparams = hparams
        self.loader_factory = loader_factory
        self.test_loader = test_loader
        self.model = model
        self.concept_dictionary = concept_dictionary
        self.class_dictionary = class_dictionary
        self.forwarder = forwarder
        self.loss_calculator = loss_calculator
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.checkpoint_manager = checkpoint_manager
        self.metrics_writer = metrics_writer

        self.concept_metrics_calc = ConceptMetricsCalculator(hparams)
        self.epoch_metrics_calc = EpochMetricsCalculator(model)
        self.decomposition_calc = DecompositionCalculator(hparams, class_predictor, concept_dictionary,
                                                          class_dictionary)

    def start(self, start_epoch: int, stop_epoch: int) -> None:
        if self.hparams.use_cuda:
            self.model = self.model.cuda()
            self.loss_calculator = self.loss_calculator.cuda()

        logger.info("Prepare test loaders")
        test_loaders = self.loader_factory.negative_test_concept_loader(sample_ratio=20)

        for epoch in range(start_epoch, stop_epoch):
            logger.info(f'epoch {epoch}')

            epoch_start_time = time.time()
            logger.info(f"[EPOCH {epoch}] Prepare train and valid loaders")
            train_loaders, val_loaders = self.loader_factory.negative_mining_loader()

            valid_concept_metrics = []

            for concept in self.loader_factory.get_concepts():
                train_loader = train_loaders[concept]
                val_loader = val_loaders[concept]
                test_loader = test_loaders[concept]

                logger.info(f"TRAIN Concept {concept}")
                self.__train(train_loader, concept, epoch)

                logger.info(f"VALID Concept {concept}")
                _, metrics = self.__validate(val_loader, concept, epoch, compute_metrics=True)
                valid_concept_metrics.append(metrics)

                logger.info(f"TEST Concept {concept}")
                scores = self.__validate(test_loader, concept, epoch)
                self.loader_factory.neg_cache[concept][1] = scores.cpu().numpy()

            logger.info("Start TEST")
            self.__test(epoch)

            epoch_time = time.time() - epoch_start_time
            epoch_metrics = self.epoch_metrics_calc.calc(valid_concept_metrics, epoch_time, self.scheduler.get_lr())

            self._scheduler_step(epoch)

            self.__save_checkpoint(epoch, score=epoch_metrics.mean_avg_precision)
            self.metrics_writer.log_epoch_metrics(epoch, epoch_metrics)

    def __train(self, dataloader: DataLoader, concept: int, epoch: int) -> None:
        self.model.train()

        self.concept_metrics_calc.reset()

        concept_name = self.concept_dictionary.get_name_by_label(concept)
        concept_idx = self.concept_dictionary.get_index_by_label(concept)

        loss_metrics = LossMetrics()
        loss_metrics.phase = "Train"
        loss_metrics.concept_name = concept_name

        for n_try in range(self.hparams.n_copy_train_set):
            for batch in tqdm(dataloader):
                if self.hparams.use_cuda:
                    batch = batch.cuda()

                self.model.zero_grad()

                model_outputs = self.forwarder.forward_train(self.model, batch, concept_id=concept_idx)

                loss, loss_metrics_ = self.loss_calculator.calc_train(model_outputs, batch, epoch)
                if n_try == self.hparams.n_copy_train_set - 1:
                    loss_metrics = loss_metrics + LossMetrics(loss_metrics_)

                loss.backward()

                if self.__on_post_backward(batch):
                    self.optimizer.step()

                if n_try == self.hparams.n_copy_train_set - 1:
                    probs = model_outputs[OutputType.MODEL_OUTPUT].sigmoid().detach()  # [B]
                    self.concept_metrics_calc.update(probs, batch)

        loss_metrics = loss_metrics / max(1, len(dataloader))

        concept_metrics = self.concept_metrics_calc.calc()
        concept_metrics.phase = "Train"
        concept_metrics.concept_name = concept_name

        if concept in self.loader_factory.get_topk_concepts(k=10):
            self.metrics_writer.log_loss_metrics(epoch, loss_metrics)
            self.metrics_writer.log_concept_metrics(epoch, concept_metrics)

    def __validate(self, dataloader: DataLoader, concept: int, epoch: int,
                   compute_metrics: bool = False) -> Tuple[torch.Tensor, Optional[ConceptMetrics]]:
        self.model.eval()

        self.concept_metrics_calc.reset()

        concept_name = self.concept_dictionary.get_name_by_label(concept)
        concept_idx = self.concept_dictionary.get_index_by_label(concept)

        loss_metrics = LossMetrics()
        loss_metrics.phase = "Valid"
        loss_metrics.concept_name = concept_name

        scores = torch.empty(0, dtype=torch.float32)
        if self.hparams.use_cuda:
            scores = scores.cuda()

        with torch.no_grad():
            for batch in tqdm(dataloader):

                if self.hparams.use_cuda:
                    batch = batch.cuda()

                model_outputs = self.forwarder.forward_val(self.model, batch, concept_id=concept_idx)
                probs = model_outputs[OutputType.MODEL_OUTPUT].sigmoid()  # [B, 1]
                # we expect that passed scores in the shape [B, 1]
                scores = torch.cat([scores, probs.squeeze(1)])

                if compute_metrics:
                    self.concept_metrics_calc.update(probs, batch)
                    _, loss_metrics_ = self.loss_calculator.calc_val(model_outputs, batch, epoch)
                    loss_metrics = loss_metrics + LossMetrics(loss_metrics_)

        if compute_metrics:
            loss_metrics = loss_metrics / max(1, len(dataloader))

            concept_metrics = self.concept_metrics_calc.calc()
            concept_metrics.phase = "Valid"
            concept_metrics.concept_name = concept_name

            if concept in self.loader_factory.get_topk_concepts(k=10):
                self.metrics_writer.log_loss_metrics(epoch, loss_metrics)
                self.metrics_writer.log_concept_metrics(epoch, concept_metrics)

            return scores, concept_metrics
        else:
            return scores

    def __test(self, epoch: int):
        self.model.eval()

        with torch.no_grad():
            self.decomposition_calc.calculate_basis(self.model)

            for batch in tqdm(self.test_loader):
                if self.hparams.use_cuda:
                    batch = batch.cuda()

                model_outputs = self.forwarder.forward_test(self.model, batch)
                logit = model_outputs[OutputType.MODEL_OUTPUT]  # [B, 7, 7, num_concepts]
                images = self.decomposition_calc.calc(batch, logit)
                self.metrics_writer.log_images(epoch, images)

    def _scheduler_step(self, epoch: int) -> None:
        self.scheduler.step(epoch)

        # Контрольный лог суммы весов сети для моделей.
        params_sum = 0.
        for p in self.model.parameters():
            params_sum += p.sum().item()

        logger.info(f'post epoch {epoch} model params sum: {params_sum}')

    def __on_post_backward(self, batch: Batch) -> bool:
        """
        Хук, вызываемый после выполнения backward и до выполнения optimizer.step.
        Возвращает флаг, разрешающий последующий optimizer.step.
        """
        # В текущем эксперименте повалили nan-ы. Для начала попробуем игнорировать такие шаги sgd, логируя
        # информацию по батчу для дальнейшего анализа.
        # Module.parameters() возвращает генератор, не список. Поэтому получаем его еще раз явно.
        for name, p in self.model.named_parameters():
            if p.grad is None:
                continue
            if torch.sum(torch.isnan(p.grad.data)).item() != 0 or torch.sum(torch.isinf(p.grad.data)).item() != 0:
                logger.warning(f'oops we have nan batch in {name}!')
                batch_paths_str = ''
                for path in batch.paths:
                    batch_paths_str += f'{path}\n'
                logger.warning(f'nan batch files: {batch_paths_str}')
                return False
        return True

    def __save_checkpoint(self, epoch: int, score: float) -> None:
        data = TrainerCheckpointParams(hparams=self.hparams, model=self.model, optimizer=self.optimizer, epoch=epoch,
                                       concept_dictionary=self.concept_dictionary, score=score,
                                       concept_cache=self.loader_factory.neg_cache)
        self.checkpoint_manager.save_checkpoint(data, less_is_better=False)
