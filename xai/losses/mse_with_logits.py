# -*- coding: utf-8 -*-

import torch
import torch.nn.functional as F

from typing import Optional
from torch.nn.modules.loss import _Loss


__all__ = ["MSEWithLogitsLoss"]


class MSEWithLogitsLoss(_Loss):

    def __init__(self, size_average: Optional[bool] = None, reduce: Optional[bool] = None, reduction: str = 'mean'):
        super().__init__(size_average=size_average, reduce=reduce, reduction=reduction)

    def forward(self, input: torch.Tensor, target: torch.Tensor):
        probs = input.sigmoid()
        return F.mse_loss(probs, target, reduction=self.reduction)
