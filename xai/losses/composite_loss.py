# -*- coding: utf-8 -*-

import torch
import logging
import torch.nn as nn

from enum import Enum
from typing import Tuple, Dict

logger = logging.getLogger(__name__)

__all__ = ["LossType", "CompositeLoss"]


class LossType(Enum):
    BCE = "bce"
    MSE = "mse"


class CompositeLoss:
    def __init__(self):
        self.loss_dict = dict()
        self.loss_coef = dict()

    def add_loss(self, name: LossType, loss: nn.Module, coef: float) -> None:
        self.loss_dict[name] = loss
        self.loss_coef[name] = coef

    def cpu(self) -> "CompositeLoss":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, nn.Module):
                self_dict[attr] = value.cpu()
        return self

    def cuda(self, **kwargs) -> "CompositeLoss":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, nn.Module):
                self_dict[attr] = value.cuda(**kwargs)
        return self

    def calculate(self, data: Dict[LossType, dict], epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        losses = {}
        result = None
        for key in self.loss_dict:
            coef = self.loss_coef[key]
            if callable(coef):
                coef = coef(epoch)

            if abs(coef) < 1e-10:
                continue

            if key in data:
                loss = self.loss_dict[key]
                loss_val = loss(**data[key]) * coef
                losses[key.value] = loss_val.cpu().item()

                if result is None:
                    result = loss_val
                else:
                    result += loss_val

            else:
                logger.error(f'No data for {key} loss')

        losses['loss'] = result.cpu().item()
        return result, losses

    def __call__(self, data: Dict[LossType, dict], epoch: int) -> Tuple[torch.Tensor, Dict[str, float]]:
        return self.calculate(data, epoch)
