# -*- coding: utf-8 -*-

from typing import List, Dict, Any
from torch.utils.data._utils.collate import default_collate

from xai.data.batch import Batch

__all__ = ["Collator"]


class Collator:

    def __call__(self, instances: List[Dict[str, Any]]) -> Batch:
        instances = default_collate(instances)
        return Batch(instances['x'], instances['path'], y=instances.get("y"), annot=instances.get("annot"),
                     grad_x=instances.get("grad_x"), probs=instances.get("probs"), image=instances.get("image"))
