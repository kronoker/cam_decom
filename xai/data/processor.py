# -*- coding: utf-8 -*-

import torchvision.transforms.functional as F

from typing import Dict, Any, List, Optional

__all__ = ["Processor", "ChainProcessor", "Normalize"]


class Processor:

    def __call__(self, instance: Dict[str, Any]) -> Dict[str, Any]:
        return instance


class ChainProcessor(Processor):

    def __init__(self, chain: List[Processor]):
        self.chain = chain

    def __call__(self, instance: Dict[str, Any]) -> Dict[str, Any]:
        for processor in self.chain:
            instance = processor(instance)

        return instance


class Normalize(Processor):

    def __init__(self, mean: List[float], std: Optional[List[float]] = None, inplace: bool = False):
        self.mean = mean
        self.std = std if std is not None else [1.] * len(self.mean)
        self.inplace = inplace

    def __call__(self, instance: Dict[str, Any]) -> Dict[str, Any]:
        instance["x"] = F.normalize(instance["x"], self.mean, self.std, self.inplace)
        return instance
