# -*- coding: utf-8 -*-

import os
import lmdb
import torch
import random
import pickle
import logging

from typing import Optional, Dict, Any, Tuple, List
from torch.utils.data import Dataset

from xai.hyperparams import Hyperparams
from xai.data.processor import Processor

logger = logging.getLogger(__name__)


__all__ = ["LmdbDataset", "ImageDataset", "ConceptDataset", "FeatureDataset"]


class LmdbDataset(Dataset):

    def __init__(self):
        self.__path2lmdb = dict()

    def __del__(self):
        self.close()

    def close(self) -> None:
        for env, cursor in self.__path2lmdb.items():
            cursor.close()
            env.close()

    def read(self, path: Tuple[str, str]) -> Dict[str, Any]:
        pair = self.__path2lmdb.get(path[0])
        if pair is None:
            env = lmdb.Environment(path[0], subdir=False, readonly=True, readahead=False, lock=False)
            cursor = env.begin(write=False).cursor()
            pair = (env, cursor)
            self.__path2lmdb[path[0]] = pair

        data = pair[1].get(path[1].encode())
        return pickle.loads(data)


class ImageDataset(LmdbDataset):

    def __init__(self, index_path: str, hparams: Hyperparams, annot_category: Optional[int] = None,
                 processor: Optional[Processor] = None):
        super().__init__()

        self.hparams = hparams
        self.processor = processor
        self.annot_category = annot_category

        self.paths = []
        index_dir = os.path.dirname(index_path)
        with open(index_path) as __fin:
            for line in __fin:
                lmdb_path, key = line.strip().split(":")
                if not os.path.isabs(lmdb_path):
                    lmdb_path = os.path.join(index_dir, lmdb_path)
                    lmdb_path = os.path.abspath(lmdb_path)
                self.paths.append((lmdb_path, key))

    def __len__(self) -> int:
        return len(self.paths)

    def __getitem__(self, index: int) -> Dict[str, Any]:
        path = self.paths[index]
        data = self.read(path)

        image = torch.from_numpy(data["image"])  # torch.uint8

        instance = {
            "path": f"{path[0]}:{path[1]}",
            "x": image / 255,  # torch.float32
            "image": image,  # torch.uint8
        }

        annot = data.get("annot")
        if annot is not None:
            if self.annot_category is not None:
                annot = annot[data["annot_cat"] == self.annot_category]
            annot = annot[random.randint(0, len(annot) - 1)]
            instance["annot"] = torch.from_numpy(annot)  # torch.int16

        if self.processor:
            instance = self.processor(instance)

        return instance


class FeatureDataset(LmdbDataset):

    def __init__(self, lmdb_path: str):
        super().__init__()

        self.paths = []
        env = lmdb.Environment(lmdb_path, subdir=False, readonly=True, readahead=False, lock=False)
        with env.begin(write=False) as tnx:
            cursor = tnx.cursor()
            for key in cursor.iternext_nodup(keys=True, values=False):
                decoced = key.decode()
                if decoced == 'meta':
                    continue
                self.paths.append((lmdb_path, decoced))
        env.close()

    def __len__(self) -> int:
        return len(self.paths)

    def __getitem__(self, index: int) -> Dict[str, Any]:
        path = self.paths[index]
        data = self.read(path)

        features = torch.from_numpy(data["features"].transpose(1, 2, 0))  # [7, 7, 512]
        grad_features = torch.from_numpy(data["grad_features"].transpose(1, 2, 0))  # [7, 7, 512]

        instance = {
            "path": f"{path[0]}:{path[1]}",
            "x": features,  # torch.float32
            "image": torch.from_numpy(data["image"]),  # torch.uint8
            "grad_x": grad_features,  # torch.float32
            "probs": torch.tensor(data["probs"]),  # torch.float32
        }

        return instance


class ConceptDataset(LmdbDataset):

    def __init__(self, samples: List[Tuple[str, int, int]], labels: List[int]):
        super().__init__()

        assert len(samples) == len(labels)
        self.samples = samples
        self.labels = labels

    def __getitem__(self, idx: int) -> Dict[str, Any]:
        image_key, i, j = self.samples[idx]
        data = self.read(image_key.split(":"))

        features = data["features"][:, i, j]
        return {
            "x": torch.from_numpy(features),  # torch.float32
            "y": torch.tensor([float(self.labels[idx])]),  # torch.float32
            "path": image_key,
        }

    def __len__(self) -> int:
        return len(self.samples)
