# -*- coding: utf-8 -*-

from typing import List, Optional
from torch import Tensor

__all__ = ["Batch"]


class Batch:
    def __init__(self, x: Tensor, paths: List[str], y: Optional[Tensor] = None, probs: Optional[Tensor] = None,
                 annot: Optional[Tensor] = None, grad_x: Optional[Tensor] = None, image: Optional[Tensor] = None):
        self.x = x
        self.paths = paths
        self.y = y
        self.annot = annot
        self.grad_x = grad_x
        self.probs = probs
        self.image = image

    def cuda(self, **kwargs) -> "Batch":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, Tensor):
                self_dict[attr] = value.cuda(**kwargs)

        return self

    def cpu(self, **kwargs) -> "Batch":
        self_dict = self.__dict__
        for attr, value in self_dict.items():
            if isinstance(value, Tensor):
                self_dict[attr] = value.cpu(**kwargs)

        return self

    @property
    def size(self) -> int:
        return self.x.shape[0]

    def __repr__(self) -> str:
        lines = []
        for attr, value in self.__dict__.items():
            if value is not None:
                lines.append(f"Attr: {attr}:")

                if isinstance(value, Tensor):
                    lines.append("Shape: {}".format(value.shape))
                lines.append(repr(value))
                lines.append("\n")

        return "\n".join(lines)
