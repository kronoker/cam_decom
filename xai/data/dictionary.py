# -*- coding: utf-8 -*-

import json

__all__ = ["Dictionary"]


class Dictionary:

    def __init__(self, path: str):
        self.path = path

        with open(path) as fin:
            data = json.load(fin)

        self.__label2name, self.__name2label, self.__label2idx, self.__idx2name = {}, {}, {}, {}
        for idx, (k, v) in enumerate(data.items()):
            k = int(k)
            self.__label2name[k] = v
            self.__name2label[v] = k
            self.__label2idx[k] = idx
            self.__idx2name[idx] = v

        assert len(self.__label2name) == len(self.__name2label) == len(self.__label2idx) == len(self.__idx2name)

    def has_label(self, label: int) -> bool:
        return label in self.__label2name

    def get_name_by_label(self, label: int) -> str:
        return self.__label2name[label]

    def get_name_by_index(self, index: int) -> str:
        return self.__idx2name[index]

    def get_index_by_label(self, label: int) -> int:
        return self.__label2idx[label]

    def get_label_by_name(self, name: str) -> int:
        return self.__name2label[name]

    def __len__(self) -> int:
        return len(self.__label2name)
