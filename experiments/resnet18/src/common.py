# -*- coding: utf-8 -*-

import os
import attr
import torch
import logging

from typing import List, Callable, Optional, Tuple
from torch.utils.data import ConcatDataset, DataLoader

from xai.hyperparams import Hyperparams
from xai.data import Processor, Normalize, ChainProcessor, ImageDataset, Collator, Dictionary, FeatureDataset
from xai.utils import set_random_seed, init_logger, Checkpoint, MetricsWriter, CheckpointManager, CheckpointDeleteParams
from xai.train import ConceptLoaderFactory
from xai.models import ResNet

logger = logging.getLogger(__name__)

# CHANGEME: Указываем символьный код эксперимента.
_EXPERIMENT_CODE = 'resnet18'


@attr.s
class ExperimentHyperparams(Hyperparams):
    loss_type = attr.ib("mse", type=str)  # bce or mse
    stage = attr.ib(0, type=int)


def get_root_path() -> str:
    root_path = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../..'))
    return root_path


def get_experiment_dir() -> str:
    root_path = get_root_path()
    exp_dir = os.path.abspath(os.path.join(root_path, f'experiments/{_EXPERIMENT_CODE}'))
    return exp_dir


def get_data_dir() -> str:
    data_dir = '/data'
    return data_dir


def get_annot_cat_dct() -> Dictionary:
    dct_path = os.path.join(get_root_path(), "resources", "annot_category.json")
    return Dictionary(dct_path)


def get_concept_dictionary() -> Dictionary:
    dct_path = os.path.join(get_root_path(), "resources", "concept_object.json")
    return Dictionary(dct_path)


def get_class_dictionary() -> Dictionary:
    dct_path = os.path.join(get_root_path(), "resources", "places365_category.json")
    return Dictionary(dct_path)


def get_hyperparams() -> ExperimentHyperparams:
    # CHANGEME: Настраиваем параметры, общие для всех запусков эксперимента.
    hparams = ExperimentHyperparams()
    concept_dct = get_concept_dictionary()

    hparams.use_cuda = True
    hparams.batch_size = 128
    hparams.num_workers = 4

    hparams.cnn_feature_map_size = (7, 7)
    hparams.clf_feature_size = 512
    hparams.num_classes = 365
    hparams.num_concepts = len(concept_dct)

    hparams.font_path = "/ibd/resources/font.ttc"
    hparams.font_size = 26
    hparams.margin = 7
    hparams.basis_num = 7

    return hparams


def get_train_image_indexes() -> List[str]:
    index_dir = os.path.join(get_data_dir(), "broden", "ready", "index_files", "object")

    return [
        os.path.join(index_dir, "index.lst"),
    ]


def get_test_image_indexes() -> List[str]:
    index_dir = os.path.join(get_data_dir(), "places_365", "ready", "index_files")

    return [
        os.path.join(index_dir, "test_index.lst"),
    ]


def get_train_features_dir() -> str:
    return os.path.join(get_data_dir(), "broden", "features")


def get_concept_idx_path() -> str:
    return os.path.join(get_train_features_dir(), "index_files", "index.pkl")


def get_test_features_dir() -> str:
    return os.path.join(get_data_dir(), "places_365", "features")


def get_test_features_lmdb_path() -> str:
    return os.path.join(get_test_features_dir(), "lmdb", "data.lmdb")


def get_class_predictor_path() -> str:
    return os.path.join("/data", "model_zoo", "resnet18_places365.pth")


def get_class_predictor(hparams: ExperimentHyperparams) -> ResNet:
    checkpoint = torch.load(get_class_predictor_path(), map_location="cpu")

    # Создаем модель на cpu
    model = ResNet(hparams)
    model.load_state_dict(checkpoint["state_dict"])
    return model


def get_image_processor(hparams: ExperimentHyperparams) -> Processor:
    processors = [
        Normalize([0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ]

    return ChainProcessor(processors)


def get_train_image_dataloader(hparams: ExperimentHyperparams, worker_init_fn: Callable) -> DataLoader:
    annot_cat_dct = get_annot_cat_dct()
    processor = get_image_processor(hparams)

    dataset = ConcatDataset([
        ImageDataset(idx_path, hparams, annot_cat_dct.get_label_by_name("object"), processor=processor)
        for idx_path in get_train_image_indexes()
    ])

    dataloader = DataLoader(dataset, batch_size=hparams.batch_size, collate_fn=Collator(), shuffle=True,
                            num_workers=hparams.num_workers, pin_memory=hparams.use_cuda, worker_init_fn=worker_init_fn)
    return dataloader


def get_test_image_dataloader(hparams: ExperimentHyperparams, worker_init_fn: Callable) -> DataLoader:
    processor = get_image_processor(hparams)

    dataset = ConcatDataset([
        ImageDataset(idx_path, hparams, processor=processor)
        for idx_path in get_test_image_indexes()
    ])

    dataloader = DataLoader(dataset, batch_size=hparams.batch_size, collate_fn=Collator(), shuffle=True,
                            num_workers=hparams.num_workers, pin_memory=hparams.use_cuda, worker_init_fn=worker_init_fn)
    return dataloader


def get_concept_loader_factory(hparams: ExperimentHyperparams, worker_initer: Callable,
                               checkpoint: Optional[Checkpoint] = None) -> ConceptLoaderFactory:
    loader_factory = ConceptLoaderFactory(get_concept_idx_path(), hparams, get_concept_dictionary(),
                                          worker_initer=worker_initer)
    if checkpoint is not None:
        loader_factory.set_neg_cache(checkpoint.data.concept_cache)
    return loader_factory


def get_test_feature_loader(hparams: ExperimentHyperparams, worker_initer: Callable) -> DataLoader:
    dataset = FeatureDataset(get_test_features_lmdb_path())
    dataloader = DataLoader(dataset, batch_size=hparams.batch_size, collate_fn=Collator(), shuffle=False,
                            num_workers=hparams.num_workers, pin_memory=hparams.use_cuda, worker_init_fn=worker_initer)
    return dataloader


def init_env(device_id: Optional[int] = None) -> None:
    """
    Инициализация окружения.
    """

    # Установка seed-а важна в том случае, если параметры сети инициализируются случайными значениями в каждом процессе.
    set_random_seed(1378)

    # Устанавливаем, с какой GPU будем работать в этом процессе.
    if device_id is not None:
        torch.cuda.set_device(device_id)


def get_log_path(exp_run_code: str) -> str:
    log_path = os.path.join(get_experiment_dir(), 'logs', exp_run_code, 'train.log')
    return log_path


def init_featurizer_logging() -> None:
    init_logger(log_level=logging.INFO, log_console=True)


def init_trainer_logging(exp_run_code: str) -> Tuple[CheckpointManager, MetricsWriter]:
    """
    Инициализируем логирование в файлы и tb, создаем менеджер чекпоинтов.
    """
    log_path = get_log_path(exp_run_code)
    log_dir = os.path.dirname(log_path)

    if not os.path.exists(log_dir):
        os.makedirs(log_dir, exist_ok=True)

    init_logger(log_level=logging.INFO, log_console=True, log_path=log_path)
    metrics_writer = MetricsWriter(log_dir)
    checkpoint_manager = CheckpointManager(get_experiment_dir(), exp_run_code,
                                           CheckpointDeleteParams(20, 50),
                                           CheckpointDeleteParams(20, 50))
    return checkpoint_manager, metrics_writer


def load_checkpoint(checkpoint_path: str) -> Checkpoint:
    checkpoint = Checkpoint()
    checkpoint.load(checkpoint_path)
    return checkpoint


class WorkerIniter:
    def __init__(self, exp_run_code: Optional[str] = None):
        self.exp_run_code = exp_run_code

    def __call__(self, worker_id: int) -> None:
        log_path = get_log_path(self.exp_run_code) if self.exp_run_code is not None else None
        init_logger(log_level=logging.INFO, log_console=True, log_path=log_path)
