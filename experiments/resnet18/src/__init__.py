import os
import sys

# Добавляем пути для поиска модулей пакета.
_root_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../..')
sys.path.append(_root_path)
