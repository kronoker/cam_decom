# -*- coding: utf-8 -*-

import os
import sys
import attr
import torch
import pprint
import logging

from typing import Optional
from torch.optim import SGD
from torch.nn import BCEWithLogitsLoss, Linear
from torch.nn.init import xavier_uniform_

# Добавляем пути для поиска модулей эксперимента.
experiment_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
sys.path.append(experiment_dir)

from src.common import (
    get_experiment_dir, get_hyperparams, get_concept_loader_factory, init_env, init_trainer_logging,
    ExperimentHyperparams, WorkerIniter, load_checkpoint, get_concept_dictionary, get_class_dictionary,
    get_test_feature_loader, get_class_predictor
)

from xai.losses import CompositeLoss, LossType, MSEWithLogitsLoss
from xai.models import ConceptClassifier, ConceptForwarder, ConceptLossCalculator
from xai.optim import ConstLRScheduler
from xai.train import Trainer
from xai.utils import CheckpointManager, Checkpoint, MetricsWriter, set_random_seed

logger = logging.getLogger()


def _train(exp_run_code: str, checkpoint_manager: CheckpointManager, metrics_writer: MetricsWriter,
           checkpoint: Optional[Checkpoint] = None, hparams: Optional[ExperimentHyperparams] = None,
           model: Optional[ConceptClassifier] = None, stop_epoch: int = 1000) -> None:
    start_epoch = 0

    concept_cache = None

    if checkpoint is not None:
        # Используем модель и гиперпараметры из чекпоинта только если они не переданы явно.
        if checkpoint.data.model:
            model = checkpoint.data.model

        if checkpoint.data.hparams:
            hparams = checkpoint.data.hparams

        if checkpoint.data.epoch is not None:
            # start_epoch = checkpoint.data.epoch + 1
            start_epoch = checkpoint.data.epoch

        concept_cache = checkpoint.data.concept_cache

    # Если загружаемся не из чекпоинта, требуется заполнить.

    concept_dct = get_concept_dictionary()
    class_dct = get_class_dictionary()
    class_predictor = get_class_predictor(hparams)

    logger.info(f"PID: {os.getpid()}")
    logger.info(f"model params count: {model.count_parameters()}")

    logger.info(pprint.pformat(attr.asdict(hparams), width=1))

    # Создаем даталоадеры.
    worker_initer = WorkerIniter(exp_run_code)

    loader_factory = get_concept_loader_factory(hparams, worker_initer, checkpoint=checkpoint)
    if concept_cache is not None:
        loader_factory.set_neg_cache(concept_cache)

    test_loader = get_test_feature_loader(hparams, worker_initer)

    logger.info(f'start_epoch: {start_epoch}')
    logger.info(f'stop_epoch: {stop_epoch}')
    assert stop_epoch > start_epoch

    if hparams.use_cuda:
        model = model.cuda()

    # Если требуется "чистый" оптимайзер (например, меняем lr), вызывающий код должен
    # присвоить None в checkpoint.data.optimizer.
    if checkpoint and checkpoint.data.optimizer:
        optimizer = checkpoint.data.optimizer
        for state in optimizer.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.cuda()
    else:
        optimizer = SGD(model.parameters(), lr=hparams.lr)

    criterion = CompositeLoss()
    if hparams.loss_type == "mse":
        criterion.add_loss(LossType.MSE, MSEWithLogitsLoss(), 1.0)
    elif hparams.loss_type == "bce":
        criterion.add_loss(LossType.BCE, BCEWithLogitsLoss(), 1.0)
    else:
        raise ValueError(f"loss_type params should be 'mse' or 'bce', not '{hparams.loss_type}'")

    loss_calculator = ConceptLossCalculator(hparams, loss=criterion)
    forwarder = ConceptForwarder(hparams)
    scheduler = ConstLRScheduler(optimizer, hparams.lr)

    trainer = Trainer(hparams, loader_factory, test_loader, model, class_predictor, concept_dct, class_dct,
                      loss_calculator, forwarder, optimizer, scheduler, checkpoint_manager, metrics_writer)

    trainer.start(start_epoch, stop_epoch)


def stage0_lr001_b128_mse() -> None:
    def __init_normalized(module) -> None:
        if type(module) == Linear:
            xavier_uniform_(module.weight)
            if hasattr(module.bias, "data"):
                module.bias.data.fill_(0.)

    init_env(device_id=0)
    set_random_seed(1378)

    # Настраиваем гиперпараметры.
    hparams = get_hyperparams()
    hparams.batch_size = 128
    hparams.lr = 1e-2
    hparams.loss_type = "mse"
    hparams.num_workers = 6

    # CHANGEME: Формируем код эксперимента.
    exp_run_code = 'stage0.lr001.b128.mse'

    # Инициализируем логирование и сохранение чекпоинтов.
    checkpoint_manager, metrics_writer = init_trainer_logging(exp_run_code)

    # Создаем модель на cpu.
    model = ConceptClassifier(hparams)
    model.apply(__init_normalized)

    logger.info(f'exp_run_code: {exp_run_code}')

    _train(exp_run_code, checkpoint_manager, metrics_writer, model=model, hparams=hparams, stop_epoch=15)


def stage0_lr001_b128_bce() -> None:
    def __init_normalized(module) -> None:
        if type(module) == Linear:
            xavier_uniform_(module.weight)
            if hasattr(module.bias, "data"):
                module.bias.data.fill_(0.)

    init_env(device_id=0)
    set_random_seed(1378)

    # Настраиваем гиперпараметры.
    hparams = get_hyperparams()
    hparams.batch_size = 128
    hparams.lr = 1e-2
    hparams.loss_type = "bce"
    hparams.num_workers = 6

    # CHANGEME: Формируем код эксперимента.
    exp_run_code = 'stage0.lr001.b128.bce'

    # Инициализируем логирование и сохранение чекпоинтов.
    checkpoint_manager, metrics_writer = init_trainer_logging(exp_run_code)

    # Создаем модель на cpu.
    model = ConceptClassifier(hparams)
    model.apply(__init_normalized)

    logger.info(f'exp_run_code: {exp_run_code}')

    _train(exp_run_code, checkpoint_manager, metrics_writer, model=model, hparams=hparams, stop_epoch=15)


def from_checkpoint() -> None:
    """
    Продолжаем без изменений упавший эксперимент.
    """
    init_env(device_id=0)
    set_random_seed(1000)

    # CHANGEME: Формируем код эксперимента, который продолжаем.
    exp_run_code = 'stage0.lr001.b128.bce'
    start_checkpoint = 'checkpoint000007.pth'

    # CHANGEME: Указываем нужный чекпоинт.
    checkpoint_path = os.path.join(get_experiment_dir(), 'models', exp_run_code, start_checkpoint)
    checkpoint = load_checkpoint(checkpoint_path)

    if checkpoint is not None and checkpoint.data.optimizer is not None:
        for param_group in checkpoint.data.optimizer.param_groups:
            param_group['lr'] = checkpoint.data.hparams.lr

    # Инициализируем логирование и сохранение чекпоинтов.
    checkpoint_manager, metrics_writer = init_trainer_logging(exp_run_code)

    logger.info(f'continue train from {checkpoint_path}')
    logger.info(f'exp_run_code: {exp_run_code}')

    _train(exp_run_code, checkpoint_manager, metrics_writer, checkpoint=checkpoint, stop_epoch=8)


def main() -> None:
    # CHANGEME: выбираем тип запуска
    from_checkpoint()


if __name__ == '__main__':
    main()
