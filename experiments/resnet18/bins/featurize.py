# -*- coding: utf-8 -*-

import os
import sys
import attr
import pprint
import logging

from typing import Optional

# Добавляем пути для поиска модулей эксперимента.
experiment_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
sys.path.append(experiment_dir)

from src.common import (
    get_experiment_dir, get_hyperparams, init_env, init_featurizer_logging, ExperimentHyperparams, WorkerIniter,
    get_train_image_dataloader, get_test_image_dataloader, get_train_features_dir, get_test_features_dir,
    get_concept_idx_path, get_class_predictor
)

from xai.utils import Checkpoint
from xai.models.resnet import ResNet, ResNetForwarder
from xai.featurizer.featurizer import Featurizer

logger = logging.getLogger(__name__)


def _featurize(checkpoint: Optional[Checkpoint] = None, hparams: Optional[ExperimentHyperparams] = None):
    if checkpoint is not None:
        if hparams is None and checkpoint.data.hparams:
            hparams = checkpoint.data.hparams

    logger.info(f"PID: {os.getpid()}")

    logger.info(pprint.pformat(attr.asdict(hparams), width=1))

    class_predictor = get_class_predictor(hparams)

    # Создаем даталоадеры.
    worker_initer = WorkerIniter()

    train_dataloader = get_train_image_dataloader(hparams, worker_initer)
    logger.info(f'Train image dataloader len: {len(train_dataloader)}')
    logger.info(f"Train image dataset len: {len(train_dataloader.dataset)}")

    test_dataloader = get_test_image_dataloader(hparams, worker_initer)
    logger.info(f'Test image dataloader len: {len(test_dataloader)}')
    logger.info(f"Test image dataset len: {len(test_dataloader.dataset)}")

    if hparams.use_cuda:
        class_predictor = class_predictor.cuda()

    forwarder = ResNetForwarder(hparams)
    # train_featurizer = Featurizer(hparams, train_dataloader, class_predictor, forwarder, get_train_features_dir(),
    #                               concept_idx_path=get_concept_idx_path())
    # train_featurizer.run()

    test_featurizer = Featurizer(hparams, test_dataloader, class_predictor, forwarder, get_test_features_dir())
    test_featurizer.run()


def from_checkpoint() -> None:
    init_env(device_id=0)

    init_featurizer_logging()

    # Настраиваем гиперпараметры.
    hparams = get_hyperparams()
    hparams.batch_size = 128
    hparams.num_classes = 365

    _featurize(hparams=hparams)


if __name__ == "__main__":
    from_checkpoint()
