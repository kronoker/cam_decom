# -*- coding: utf-8 -*-

import os
import lmdb
import pickle
import numpy as np

from typing import List
from tqdm.auto import tqdm
from PIL import Image
from argparse import ArgumentParser, Namespace


"""
    Example run command: 
    python utils/datasets/convert_places.py --in_index_dir '/data/in_index_files' --lmdb_path '/data/data.lmdb' \
     --out_index_dir '/data/out_index_files'
"""


def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--in_index_dir", help="Path to directory with source index files", required=True, type=str)
    parser.add_argument("--lmdb_path", help="Path to output lmdb file", required=True, type=str)
    parser.add_argument("--out_index_dir", help="Path to directory with output index files", required=True, type=str)

    return parser.parse_args()


def read_image(image_path: str) -> np.ndarray:
    with Image.open(image_path) as image:
        matrix = np.asarray(image, dtype=np.uint8)
    if matrix.ndim == 2:
        matrix = np.stack((matrix,) * 3, axis=-1)  # [H, W] -> [H, W, C]
    matrix = matrix.transpose((2, 0, 1))  # [H, W, C] -> [C, H, W]
    return matrix


def create_parent(path: str, isfile: bool = True) -> None:
    directory = os.path.dirname(path) if isfile else path
    if directory:
        os.makedirs(directory, exist_ok=True)


def read_index(index_path: str) -> List[str]:
    index_dir = os.path.dirname(index_path)
    result = []
    with open(index_path) as __fin:
        for line in __fin:
            path = line.strip()
            if not os.path.isabs(path):
                path = os.path.join(index_dir, path)
                path = os.path.abspath(path)
            result.append(path)
    return result


def create_lmdb(in_index_dir: str, lmdb_path: str, out_index_dir: str) -> None:
    create_parent(lmdb_path)
    create_parent(out_index_dir, isfile=False)

    env = lmdb.Environment(lmdb_path, map_size=1024 ** 4, subdir=False, readahead=False, max_dbs=0,
                           writemap=False, meminit=False, map_async=True, lock=False)

    with env.begin(write=True) as tnx:
        for index_name in os.listdir(in_index_dir):
            in_index_path = os.path.join(in_index_dir, index_name)
            out_index_path = os.path.join(out_index_dir, index_name)
            rel_path = os.path.relpath(lmdb_path, os.path.dirname(out_index_path))

            paths = read_index(in_index_path)

            with open(out_index_path, 'w') as __fout:
                for path in tqdm(paths):
                    lmdb_key = os.path.basename(path).split(".")[0]
                    lmdb_value = {
                        "image": read_image(path),
                    }
                    tnx.put(lmdb_key.encode(), pickle.dumps(lmdb_value), dupdata=False, overwrite=True)

                    __fout.write(f"{rel_path}:{lmdb_key}\n")

    env.close()


def main() -> None:
    args = get_args()
    create_lmdb(args.in_index_dir, args.lmdb_path, args.out_index_dir)


if __name__ == "__main__":
    main()
