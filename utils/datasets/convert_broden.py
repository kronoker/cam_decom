# -*- coding: utf-8 -*-

import os
import sys
import csv
import lmdb
import pickle
import numpy as np

from PIL import Image
from tqdm.auto import tqdm
from argparse import ArgumentParser, Namespace

# CHANGEME: Корневая директория внутри контейнера
ROOT_DIR: str = "/ibd"

sys.path.append(ROOT_DIR)
from xai.data.dictionary import Dictionary

"""
    Example run command: 
    python utils/datasets/convert_broden.py --source_dir '/data/source' --lmdb_path '/data/data.lmdb' \
     --index_dir '/data/index_files'

    Expected directory structure in 'source_dir'
    - images/ - directory of images and annotations
    - index.csv - index csv file
"""


def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--source_dir", help="Path to input directory with source data", required=True, type=str)
    parser.add_argument("--lmdb_path", help="Path to output lmdb file", required=True, type=str)
    parser.add_argument("--index_dir", help="Path to output directory with index files", required=True, type=str)

    return parser.parse_args()


def read_image(image_path: str) -> np.ndarray:
    with Image.open(image_path) as image:
        matrix = np.asarray(image, dtype=np.uint8)
    if matrix.ndim == 2:
        matrix = np.stack((matrix,) * 3, axis=-1)  # [H, W] -> [H, W, C]
    matrix = matrix.transpose((2, 0, 1))  # [H, W, C] -> [C, H, W]
    return matrix


def read_annot(annot_path: str, concept_dct: Dictionary) -> np.ndarray:
    with Image.open(annot_path) as image:
        matrix = np.asarray(image, dtype=np.int16)
    if matrix.ndim >= 2:
        return matrix[:, :, 0] + matrix[:, :, 1] * 256
    numbers = np.unique(matrix)
    for number in numbers:
        if not concept_dct.has_label(number):
            matrix[matrix == number] = 0
    return matrix


def create_parent(path: str, isfile: bool = True) -> None:
    directory = os.path.dirname(path) if isfile else path
    if directory and not os.path.isdir(directory):
        os.makedirs(directory, exist_ok=True)


def create_lmdb(source_dir: str, lmdb_path: str, index_dir: str) -> None:
    index_csv = os.path.join(source_dir, "index.csv")

    create_parent(lmdb_path)

    obj_idx_dir = os.path.join(index_dir, "object")
    obj_train_idx_path = os.path.join(obj_idx_dir, "train.lst")
    create_parent(obj_train_idx_path)
    obj_valid_idx_path = os.path.join(obj_idx_dir, "valid.lst")
    create_parent(obj_valid_idx_path)

    part_idx_dir = os.path.join(index_dir, "part")
    part_train_idx_path = os.path.join(part_idx_dir, "train.lst")
    create_parent(part_train_idx_path)
    part_valid_idx_path = os.path.join(part_idx_dir, "valid.lst")
    create_parent(part_valid_idx_path)

    create_parent(index_dir, isfile=False)

    annot_cat_dct_path = os.path.join(ROOT_DIR, "resources", "annot_category.json")
    concept_dct_path = os.path.join(ROOT_DIR, "resources", "concept_all.json")

    annot_cat_dct = Dictionary(annot_cat_dct_path)
    concept_dct = Dictionary(concept_dct_path)

    env = lmdb.Environment(lmdb_path, map_size=1024 ** 4, subdir=False, readahead=False, max_dbs=0,
                           writemap=False, meminit=False, map_async=True, lock=False)
    with env.begin(write=True) as tnx, \
         open(index_csv) as __fin, \
         open(obj_train_idx_path, 'w') as obj_train_idx, \
         open(obj_valid_idx_path, 'w') as obj_valid_idx, \
         open(part_train_idx_path, 'w') as part_train_idx, \
         open(part_valid_idx_path, 'w') as part_valid_idx:
        reader = csv.DictReader(__fin)
        for row in tqdm(reader):
            image_path = row["image"]
            full_image_path = os.path.join(source_dir, "images", image_path)
            if not os.path.isfile(full_image_path):
                continue

            object_annot_path = os.path.join(source_dir, "images", row["object"])
            part_annot_paths = []
            for path in row["part"].split(";"):
                path = os.path.join(source_dir, "images", path)
                if os.path.isfile(path):
                    part_annot_paths.append(path)

            if not os.path.isfile(object_annot_path) and len(part_annot_paths) == 0:
                continue

            value = {
                "image": read_image(full_image_path),
            }

            annot = np.empty((0, 112, 112), dtype=np.int16)
            annot_cat = np.empty(0, dtype=np.int8)

            if os.path.isfile(object_annot_path):
                object_annot = read_annot(object_annot_path, concept_dct)
                if len(np.unique(object_annot)) > 1:
                    annot_cat = np.append(annot_cat, annot_cat_dct.get_label_by_name("object"))
                    annot = np.append(annot, np.expand_dims(object_annot, 0), axis=0)

            for path in part_annot_paths:
                part_annot = read_annot(path, concept_dct)
                if len(np.unique(part_annot)) > 1:
                    annot_cat = np.append(annot_cat, annot_cat_dct.get_label_by_name("part"))
                    annot = np.append(annot, np.expand_dims(part_annot, 0), axis=0)

            if annot.shape[0] == 0:
                continue

            value["annot"] = annot
            value["annot_cat"] = annot_cat

            key = image_path.split(".")[0]
            value = pickle.dumps(value)
            tnx.put(key.encode(), value, dupdata=False, overwrite=True)

            has_object = (annot_cat == annot_cat_dct.get_label_by_name("object")).any()
            has_part = (annot_cat == annot_cat_dct.get_label_by_name("part")).any()

            obj_line = f"{os.path.relpath(lmdb_path, obj_idx_dir)}:{key}\n"
            part_line = f"{os.path.relpath(lmdb_path, part_idx_dir)}:{key}\n"

            if row["split"] == "train":
                if has_object:
                    obj_train_idx.write(obj_line)
                if has_part:
                    part_train_idx.write(part_line)
            else:
                if has_object:
                    obj_valid_idx.write(obj_line)
                if has_part:
                    part_valid_idx.write(part_line)

    env.close()


def main() -> None:
    args = get_args()
    create_lmdb(args.source_dir, args.lmdb_path, args.index_dir)


if __name__ == "__main__":
    main()
